package com.peller.aizenberg.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

fun Activity.hideKeyboard() {
    val view = if (currentFocus == null) View(this) else currentFocus!!
    hideKeyboard(view)
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun String.removeSpecialCharacters() : String {
    return this.replace(Regex("[^a-zA-Z0-9\\s+]"), "")
}

