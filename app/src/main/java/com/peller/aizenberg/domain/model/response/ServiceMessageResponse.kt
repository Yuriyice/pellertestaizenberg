package com.peller.aizenberg.domain.model.response

import com.google.firebase.messaging.RemoteMessage
import com.peller.aizenberg.utils.Configuration

data class ServiceMessageResponse(val message: String, val from: String) {

    fun isOwnerOfMessage(startWith: String) : Boolean {
        return from.startsWith(startWith, true)
    }

    fun isMessageFromUser() : Boolean {
        return isOwnerOfMessage(Configuration.USER_NAME)
    }

    companion object {

        private const val FCM_MESSAGE_KEY = "gcm.notification.body"
        private const val FCM_USERNAME_KEY = "gcm.notification.username"

        fun createFromRemoteMessage(remoteMessage: RemoteMessage) : ServiceMessageResponse? {
            val extras = remoteMessage.toIntent().extras ?: return null
            return ServiceMessageResponse(extras.getString(FCM_MESSAGE_KEY) ?: "", extras.getString(
                FCM_USERNAME_KEY) ?: "")
        }
    }
}