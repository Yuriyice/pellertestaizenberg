package com.peller.aizenberg.domain.interactor

interface IConnectChatListener {

    fun onConnectionEstablished(token: String)

    fun onConnectionFailure()


}