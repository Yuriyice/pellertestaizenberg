package com.peller.aizenberg.domain.interactor

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.peller.aizenberg.utils.Configuration
import com.peller.aizenberg.domain.repository.IMessageRepository
import com.peller.aizenberg.domain.model.response.FCMResponse
import com.peller.aizenberg.utils.withMultithreadProcessor
import io.reactivex.Single

class MainInteractor(private val messageRepository: IMessageRepository) {

    fun send(body: String, from: String): Single<FCMResponse> {
        return messageRepository.send(body, from).withMultithreadProcessor()
    }

    fun connect(listener: IConnectChatListener) {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (it.isSuccessful) {
                listener.onConnectionEstablished(it.result!!.token)
            } else {
                listener.onConnectionFailure()
            }
        }
    }

    fun subscribe(listener: ITopicSubscriberListener) {
        FirebaseMessaging.getInstance().subscribeToTopic(Configuration.TOPIC_NAME)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    listener.onSubscribed()
                } else {
                    listener.onFailure()
                }
            }
    }

    fun unsubscribe(listener: ITopicSubscriberListener) {
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Configuration.TOPIC_NAME)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    listener.onSubscribed()
                } else {
                    listener.onFailure()
                }
            }
    }

}