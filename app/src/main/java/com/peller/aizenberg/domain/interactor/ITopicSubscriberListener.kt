package com.peller.aizenberg.domain.interactor

interface ITopicSubscriberListener {

    fun onSubscribed()

    fun onUnsubscribed()

    fun onFailure()

}