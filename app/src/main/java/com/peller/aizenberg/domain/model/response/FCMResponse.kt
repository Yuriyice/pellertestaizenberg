package com.peller.aizenberg.domain.model.response

data class FCMResponse(val success: Int?, val message_id: String?) {


    fun isSuccess() : Boolean {
        return (success == null || success == 1) && message_id != null
    }

}