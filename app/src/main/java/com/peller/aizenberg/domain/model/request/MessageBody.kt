package com.peller.aizenberg.domain.model.request

data class MessageBody(val notification: MessageBodyInternal, val to: String)