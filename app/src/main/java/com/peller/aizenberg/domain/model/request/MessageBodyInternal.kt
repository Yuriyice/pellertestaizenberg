package com.peller.aizenberg.domain.model.request

data class MessageBodyInternal(val body: String, val username: String)