package com.peller.aizenberg.domain.repository

import com.peller.aizenberg.domain.model.response.FCMResponse
import io.reactivex.Single

interface IMessageRepository {

    fun send(body: String, from: String) : Single<FCMResponse>

}