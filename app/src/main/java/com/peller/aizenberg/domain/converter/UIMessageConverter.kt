package com.peller.aizenberg.domain.converter

import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.ui.model.UIMessage

object UIMessageConverter  {

    fun convertToUIMessage(serviceMessageResponse: ServiceMessageResponse) : UIMessage {
        return UIMessage(serviceMessageResponse.message, serviceMessageResponse.from)
    }

}