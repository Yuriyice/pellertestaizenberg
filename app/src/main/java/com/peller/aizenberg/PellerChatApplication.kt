package com.peller.aizenberg

import android.app.Application
import com.peller.aizenberg.di.interactor.InteractorKoinModule
import com.peller.aizenberg.di.network.ApiKoinModule
import com.peller.aizenberg.di.network.CommonNetworkKoinModule
import com.peller.aizenberg.di.network.NetworkCoinModule
import com.peller.aizenberg.di.repository.RepositoryKoinModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class PellerChatApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        configureDI()
        configureLogger()
    }

    companion object {
        private lateinit var instance: PellerChatApplication

        fun getInstance(): PellerChatApplication = instance

    }

    private fun configureDI() {
        startKoin {
            androidContext(this@PellerChatApplication)
            modules(
                arrayListOf(
                    InteractorKoinModule.getModule(),
                    CommonNetworkKoinModule.getModule(),
                    NetworkCoinModule.getModule(),
                    RepositoryKoinModule.getModule(),
                    ApiKoinModule.getModule()
                )
            )
        }
    }

    private fun configureLogger() {
        Timber.plant(Timber.DebugTree())
    }

}