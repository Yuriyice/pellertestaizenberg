package com.peller.aizenberg.ui.model

interface IUIMessage {

    fun getMessageType() : UIMessageType

    enum class UIMessageType {
        INCOMING,
        OUTGOING,
        BOT_CONNECTED,
        BOT_DISCONNECTED
    }

}