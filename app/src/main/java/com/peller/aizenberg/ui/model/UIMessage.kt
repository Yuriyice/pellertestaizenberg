package com.peller.aizenberg.ui.model

import com.google.firebase.messaging.RemoteMessage
import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.utils.Configuration

data class UIMessage(val message: String, val from: String) : IUIMessage {

    override fun getMessageType(): IUIMessage.UIMessageType {
        return if (isMessageFromUser()) IUIMessage.UIMessageType.OUTGOING else IUIMessage.UIMessageType.INCOMING
    }

    fun isOwnerOfMessage(startWith: String) : Boolean {
        return from.startsWith(startWith, true)
    }

    fun isMessageFromUser() : Boolean {
        return isOwnerOfMessage(Configuration.USER_NAME)
    }

    fun displayName() = if (isMessageFromUser()) "Me ($from)" else from

    companion object {

        private const val FCM_MESSAGE_KEY = "gcm.notification.body"
        private const val FCM_USERNAME_KEY = "gcm.notification.username"

        fun createFromRemoteMessage(remoteMessage: RemoteMessage) : ServiceMessageResponse? {
            val extras = remoteMessage.toIntent().extras ?: return null
            return ServiceMessageResponse(extras.getString(FCM_MESSAGE_KEY) ?: "", extras.getString(
                FCM_USERNAME_KEY) ?: "")
        }
    }
}