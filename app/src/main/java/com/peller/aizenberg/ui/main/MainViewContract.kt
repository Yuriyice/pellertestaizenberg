package com.peller.aizenberg.ui.main

import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.ui.model.UIMessage
import moxy.MvpView
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

@StateStrategyType(value = SkipStrategy::class)
interface MainViewContract : MvpView {

    fun enableMessageBlock()

    fun disableMessageBlock()

    fun showProgress()

    //TODO add error reason
    fun onError()

    fun hideProgress()

    fun onNewMessage(messageResponse: UIMessage)

    fun onMessageSentFailure()

    fun clearMessageBlock()


}