package com.peller.aizenberg.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.peller.aizenberg.R
import com.peller.aizenberg.data.bot.BotManager
import com.peller.aizenberg.data.bot.BotNamesManager
import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.ui.adapter.ChatAdapter
import com.peller.aizenberg.ui.adapter.ItemOffsetDecoration
import com.peller.aizenberg.ui.main.MainPresenter
import com.peller.aizenberg.ui.main.MainViewContract
import com.peller.aizenberg.ui.model.RoomActionMessageModel
import com.peller.aizenberg.ui.model.UIMessage
import com.peller.aizenberg.ui.view.SendMessageView
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

class MainActivity : MvpAppCompatActivity(), MainViewContract {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    private lateinit var sendMessageView: SendMessageView
    private lateinit var rvMessages: RecyclerView
    private lateinit var imgNoMessages: View
    private lateinit var txtViewState: TextView

    private var chatAdapter : ChatAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendMessageView = findViewById(R.id.sendMessageView)
        rvMessages = findViewById(R.id.rvMessages)
        imgNoMessages = findViewById(R.id.imgNoMessages)

        findViewById<View>(R.id.imgRemoveBot).setOnClickListener { removeBot() }
        findViewById<View>(R.id.imgAddBot).setOnClickListener { addBot() }
        txtViewState = findViewById(R.id.txtViewState)

        sendMessageView.deactivateSendButton()

        sendMessageView.callback = object : SendMessageView.ISendMessageViewCallback {
            override fun onSendClick(data: String) {
                sendMessageView.hideKeyboard()
                presenter.sendMessage(data)
            }

            override fun onInputFieldChanged(data: String) {
                if (data.isNotBlank()) {
                    sendMessageView.activateSendButton()
                } else {
                    sendMessageView.deactivateSendButton()
                }
            }

        }

        presenter.prepare()

        rvMessages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        chatAdapter = ChatAdapter(this)
        rvMessages.adapter = chatAdapter
    }

    override fun onStop() {
        BotManager.dispose()
        super.onStop()
    }

    private fun hideLogo() {
        val alphaAnimation = AlphaAnimation(1f, 0.1f)
        alphaAnimation.duration = 400
        alphaAnimation.fillAfter = true
        imgNoMessages.startAnimation(alphaAnimation)
    }

    private fun addBot() {
        if (chatAdapter?.isEmpty() == true) {
            hideLogo()
        }
//        imgNoMessages.visibility = View.GONE
        BotManager.createBot()?.let {
            chatAdapter?.addData(RoomActionMessageModel(it.botDisplayName, true), 0)
        }
        updateBotCount()
    }

    private fun removeBot() {
        BotManager.destroyBot()?.let {
            chatAdapter?.addData(RoomActionMessageModel(it.botDisplayName, false), 0)
        }
        updateBotCount()
    }

    @SuppressLint("SetTextI18n")
    private fun updateBotCount() {
        txtViewState.text = "${BotManager.botsCount()}/${BotManager.MAX_BOT_COUNT}"
    }

    override fun enableMessageBlock() {
        sendMessageView.unlock()
    }

    override fun disableMessageBlock() {
        sendMessageView.lock()
    }

    override fun showProgress() {
        sendMessageView.showProgress()
    }

    override fun onError() {
        Toast.makeText(this, "Something went wrong :(", Toast.LENGTH_LONG).show()
    }

    override fun hideProgress() {
        sendMessageView.hideProgress()
    }

    override fun onNewMessage(messageResponse: UIMessage) {
        //TODO refactor this
        runOnUiThread {
//            imgNoMessages.visibility = View.GONE
            if (chatAdapter?.isEmpty() == true) {
                hideLogo()
            }
            chatAdapter?.addData(messageResponse, 0)
        }
    }

    override fun onMessageSentFailure() {
        Toast.makeText(this, "Something went wrong :(", Toast.LENGTH_LONG).show()
    }

    override fun clearMessageBlock() {
        sendMessageView.clear()
    }
}
