package com.peller.aizenberg.ui.model

data class RoomActionMessageModel(val botDisplayName: String, val isConnected: Boolean) : IUIMessage {

    override fun getMessageType(): IUIMessage.UIMessageType {
        return if (isConnected) IUIMessage.UIMessageType.BOT_CONNECTED else IUIMessage.UIMessageType.BOT_DISCONNECTED
    }

    fun getUILabel() : String {
        return if (!isConnected)
            "Bot $botDisplayName left the chat" else "Bot $botDisplayName join the chat"
    }

}