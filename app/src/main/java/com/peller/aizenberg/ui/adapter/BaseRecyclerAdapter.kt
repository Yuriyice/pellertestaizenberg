package com.peller.aizenberg.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by Yuriy Aizenberg
 */
abstract class BaseRecyclerAdapter<T, VH : BaseRecyclerAdapter.VHolder>(protected val context: Context) :
    RecyclerView.Adapter<VH>() {

    var data: MutableList<T> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    open fun addData(data: List<T>) {
        if (data.isEmpty()) return
        val position = this.data.size
        this.data.addAll(data)
        if (position == 0) notifyDataSetChanged() else notifyItemRangeInserted(position, data.size)
    }

    open fun addData(data: List<T>, position: Int) {
        if (data.isEmpty()) return
        if (this.data.isEmpty()) {
            addData(data)
        } else {
            this.data.addAll(position, data)
            if (position == 0) {
                notifyDataSetChanged()
            } else {
                notifyItemRangeInserted(position, data.size)
            }
        }
    }

    open fun removeAt(position: Int) {
        (data as ArrayList).removeAt(position)
        if (position != 0) {
            notifyItemRemoved(position)
        } else {
            notifyDataSetChanged()
        }
    }

    fun addNewData(data: List<T>) {
        if (data.isEmpty()) return
        val position = this.data.size
        this.data.addAll(data)
        if (position == 0) notifyDataSetChanged() else {
            notifyItemRangeInserted(position, data.size)
            notifyItemRangeChanged(position, this.data.size)
        }
    }

    fun addNewData(data: List<T>, position: Int) {
        if (data.isEmpty()) return
        if (this.data.isEmpty()) {
            addData(data)
        } else {
            this.data.addAll(position, data)
            notifyItemRangeInserted(position, data.size)
        }
        notifyDataSetChanged()
    }

    open fun addData(data: T) {
        val position = this.data.size
        this.data.add(data)
        if (position == 0) notifyDataSetChanged() else notifyItemInserted(this.data.size)
    }

    open fun addData(data: T, position: Int) {
        if (this.data.isEmpty()) {
            addData(data)
        } else {
            this.data.add(position, data)
            if (position != 0) {
                notifyItemInserted(position)
            } else {
                notifyDataSetChanged()
            }
        }
    }


    fun replaceItem(data: T, position: Int) {
        if (this.data.isEmpty()) {
            addData(data)
        } else {
            this.data.set(position, data)
            if (position == 0) {
                notifyDataSetChanged()
            } else {
                notifyItemChanged(position)
            }
        }
    }


    fun isEmpty(): Boolean = data.isEmpty()

    fun getItem(position: Int): T = data[position]

    protected fun inflate(@LayoutRes resId: Int, viewGroup: ViewGroup): View {
        return (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
            resId,
            viewGroup,
            false
        )
    }

    protected fun unbindTouchListener(view: View) {
        view.setOnClickListener(null)
    }

    override fun getItemCount(): Int = data.size

    override fun getItemId(position: Int): Long = position.toLong()

    fun clear() {
        data.clear()
        notifyDataSetChanged()
    }

    abstract class VHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun <T : View> findView(@IdRes id: Int): T = itemView.findViewById(id)

    }
}