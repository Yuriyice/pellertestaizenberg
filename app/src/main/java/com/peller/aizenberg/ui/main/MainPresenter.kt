package com.peller.aizenberg.ui.main

import com.peller.aizenberg.di.KoinInjector
import com.peller.aizenberg.domain.converter.UIMessageConverter
import com.peller.aizenberg.domain.interactor.IConnectChatListener
import com.peller.aizenberg.domain.interactor.ITopicSubscriberListener
import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.service.ServiceBridge
import com.peller.aizenberg.utils.Configuration
import io.reactivex.disposables.CompositeDisposable
import moxy.InjectViewState
import moxy.MvpPresenter
import okhttp3.internal.http.BridgeInterceptor

@InjectViewState
class MainPresenter : MvpPresenter<MainViewContract>(), ServiceBridge.IBridgeListener, ITopicSubscriberListener {

    private val interactor = KoinInjector.instance.mainInteractor
    private val compositeDisposable = CompositeDisposable()


    fun prepare() {
        viewState.disableMessageBlock()
        interactor.connect(object : IConnectChatListener {
            override fun onConnectionEstablished(token: String) {
                subscribeToTopic()
            }

            override fun onConnectionFailure() {
                viewState.disableMessageBlock()
                viewState.onError()
            }

        })

        ServiceBridge.subscribe(this)
    }

    override fun onDestroy() {
        dispose()
        super.onDestroy()
    }


    fun sendMessage(message: String) {
        viewState.showProgress()
        viewState.disableMessageBlock()
        compositeDisposable.add(interactor.send(message, Configuration.USER_NAME).subscribe({
            viewState.clearMessageBlock()
            viewState.hideProgress()
            viewState.enableMessageBlock()
        }, {
            viewState.enableMessageBlock()
            viewState.hideProgress()
            viewState.onError()
        }))
    }

    private fun subscribeToTopic() {
        interactor.subscribe(this)
    }

    override fun onMessageReceived(response: ServiceMessageResponse) {
        //TODO move message subscription to interactor!!!
        viewState.onNewMessage(UIMessageConverter.convertToUIMessage(response))
    }

    override fun dispose() {
        interactor.unsubscribe(this)
    }

    override fun onSubscribed() {
        viewState.enableMessageBlock()
    }

    override fun onUnsubscribed() {
        //Nothing
    }

    override fun onFailure() {
        viewState.disableMessageBlock()
        viewState.onError()
    }


}