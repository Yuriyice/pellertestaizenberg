package com.peller.aizenberg.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.peller.aizenberg.R
import com.peller.aizenberg.utils.hideKeyboard

class SendMessageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val textInputLayout: TextInputLayout
    private val imgSendMessage: ImageView
    private val edtChatMessage: TextInputEditText
    private val progressSendMessage: View
    var callback : ISendMessageViewCallback? = null

    init {
        inflate(context, R.layout.view_send_message, this)
        textInputLayout = findViewById(R.id.inputLayout)
        imgSendMessage = findViewById(R.id.imgSendMessage)
        edtChatMessage = findViewById(R.id.edtChatMessage)
        progressSendMessage = findViewById(R.id.progressSendMessage)
        imgSendMessage.setOnClickListener {
            val text = edtChatMessage.text ?: return@setOnClickListener
            if (text.isNotBlank()) {
                callback?.onSendClick(text.toString())
            }
        }
        edtChatMessage.addTextChangedListener {
            it?.run {
                callback?.onInputFieldChanged(toString())
            }
        }

    }

    fun hideKeyboard() {
        context.hideKeyboard(edtChatMessage)
    }

    fun showProgress() {
        changeProgressVisibility(true)
    }

    fun hideProgress() {
        changeProgressVisibility(false)

    }

    fun changeProgressVisibility(show: Boolean) {
        imgSendMessage.visibility = if (show) View.INVISIBLE else View.VISIBLE
        progressSendMessage.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun activateSendButton() {
        imgSendMessage.setImageResource(R.drawable.ic_send_active)
    }

    fun deactivateSendButton() {
        imgSendMessage.setImageResource(R.drawable.ic_send_locked)
    }

    fun lock() {
        changeLockStateInternal(true)
    }

    fun unlock() {
        changeLockStateInternal(false)
    }

    private fun changeLockStateInternal(isLocked: Boolean) {
        textInputLayout.isEnabled = !isLocked
        imgSendMessage.isEnabled = !isLocked
    }

    fun clear() {
        edtChatMessage.text = null
    }


    interface ISendMessageViewCallback {
        fun onSendClick(data: String)

        fun onInputFieldChanged(data: String)

    }
}