package com.peller.aizenberg.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.ivbaranov.mli.MaterialLetterIcon
import com.peller.aizenberg.R
import com.peller.aizenberg.ui.model.IUIMessage
import com.peller.aizenberg.ui.model.IUIMessage.UIMessageType.*
import com.peller.aizenberg.ui.model.RoomActionMessageModel
import com.peller.aizenberg.ui.model.UIMessage

class ChatAdapter(context: Context) :
    BaseRecyclerAdapter<IUIMessage, ChatAdapter.ChatVHolder>(context) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatVHolder {

        val uiMessageType = values()[viewType]
        return if (uiMessageType == BOT_CONNECTED || uiMessageType == BOT_DISCONNECTED) {
            ChatActionVHolder(inflate(R.layout.item_room_action, parent))
        } else {
            ChatMessageVHolder(inflate(if (uiMessageType == INCOMING) R.layout.item_chat_message_incoming else R.layout.item_chat_message_outgoing, parent))
        }
    }

    override fun onBindViewHolder(holder: ChatVHolder, position: Int) {
        val item = getItem(position)

        when(values()[getItemViewType(position)]) {
            INCOMING,OUTGOING -> bindMessageHolder(holder as ChatMessageVHolder, item as UIMessage)
            BOT_CONNECTED, BOT_DISCONNECTED -> bindActionHolder(holder as ChatActionVHolder, item as RoomActionMessageModel)
        }
    }


    private fun bindMessageHolder(holder: ChatMessageVHolder, message: UIMessage) {
        holder.imgUserAvatar.letter = message.from.first().toString()
        holder.txtUserName.text = message.displayName()
        holder.txtChatText.text = message.message
    }

    private fun bindActionHolder(holder: ChatActionVHolder, message: RoomActionMessageModel) {
        holder.txtRoomAction.text = message.getUILabel()
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).getMessageType().ordinal
    }



    open class ChatVHolder(itemView: View) : VHolder(itemView)



    class ChatMessageVHolder(itemView: View) : ChatVHolder(itemView) {
        val imgUserAvatar: MaterialLetterIcon = findView(R.id.imgUserAvatar)
        val txtUserName: TextView = findView(R.id.txtUserName)
        val txtChatText: TextView = findView(R.id.txtChatText)
    }

    class ChatActionVHolder(itemView: View) : ChatVHolder(itemView) {
        val txtRoomAction: TextView = findView(R.id.txtRoomAction)
    }
}