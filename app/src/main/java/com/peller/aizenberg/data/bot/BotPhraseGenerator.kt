package com.peller.aizenberg.data.bot

import com.peller.aizenberg.utils.removeSpecialCharacters
import java.util.*
import kotlin.collections.ArrayList

object BotPhraseGenerator {

    private val wordsList = ArrayList<String>()

    init {
        val pipeline = "\n" +
                "\n" +
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus at nibh neque. Sed sodales elit purus, non facilisis risus mattis non. Etiam consectetur risus ut urna tincidunt, eu sodales augue bibendum. Praesent a ullamcorper velit. Vestibulum gravida tincidunt velit, ut vehicula risus egestas et. Mauris suscipit ligula at nisi pharetra, at tincidunt diam dignissim. Praesent feugiat porttitor orci eu fermentum. Curabitur sagittis massa eget velit rhoncus pulvinar. Vivamus maximus lorem non turpis dictum pellentesque.\n" +
                "\n" +
                "Cras pharetra mauris a nunc rhoncus facilisis. Vivamus vitae facilisis diam. Vivamus viverra lorem massa, at convallis dui vestibulum vel. Fusce sodales, ante et congue bibendum, nisl sem finibus ante, non lobortis felis massa ut dui. Pellentesque et ante pellentesque, fringilla dolor aliquet, lobortis mi. Integer fermentum sem orci, a vehicula neque commodo hendrerit. Nulla euismod lorem quis commodo tristique. Suspendisse at finibus arcu, non efficitur leo. Nulla quis mi massa. Vestibulum ultricies massa leo. Donec eget faucibus tellus. Donec placerat ac eros id tristique. Proin ut semper enim. Nullam at mi ultricies, consequat elit eleifend, egestas risus.\n" +
                "\n" +
                "Integer eu molestie augue, ut dapibus purus. Aliquam sem purus, maximus eu turpis at, malesuada imperdiet tortor. Fusce eget quam sed dui vulputate posuere nec eu mi. In hac habitasse platea dictumst. Duis orci dui, lobortis vel tortor id, feugiat interdum dolor. Nullam nec nunc velit. Nulla dignissim congue libero, at lobortis velit posuere sed. Donec efficitur mollis elit. Curabitur feugiat orci sit amet lectus interdum, quis convallis est auctor. Etiam placerat justo a aliquam sodales. Sed commodo maximus nulla ac euismod. Donec varius faucibus efficitur. Cras eu arcu nec leo tincidunt gravida eget non lectus.\n" +
                "\n" +
                "Aliquam a rutrum quam, laoreet feugiat enim. Sed ac finibus est, eget mattis arcu. Cras ornare ligula et justo imperdiet, vel ultrices tortor molestie. Morbi ut nisl iaculis, euismod libero a, sodales sapien. Cras luctus viverra ornare. Integer vel pretium nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;\n" +
                "\n" +
                "Donec in quam nec mauris aliquet vulputate vel vitae sapien. Vivamus vel elementum ligula. Etiam vitae aliquet eros. Donec efficitur feugiat molestie. Suspendisse mollis quam quis velit suscipit, pulvinar pellentesque quam ornare. In at pharetra lectus, et laoreet ex. Nulla facilisi. Suspendisse at nunc tincidunt, scelerisque quam eget, dapibus libero. Ut quis arcu sit amet odio bibendum venenatis. Quisque consequat rhoncus ligula, sed sodales tellus dapibus vel. Donec varius tempor nisl et imperdiet. Ut vitae vulputate elit. Ut ex neque, feugiat vitae neque et, feugiat euismod ligula. Sed tincidunt, lacus vel venenatis tincidunt, mi tortor facilisis libero, ac dictum metus ex ut quam. ";
        wordsList.addAll(pipeline.split(" "));
    }

    fun randomTwoWords() : String {
        return "${wordsList.random().removeSpecialCharacters().capitalize()} ${wordsList.random().removeSpecialCharacters().toLowerCase(
            Locale.getDefault()
        )}"
    }





}
