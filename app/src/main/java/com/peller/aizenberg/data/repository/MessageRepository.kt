package com.peller.aizenberg.data.repository

import com.peller.aizenberg.utils.Configuration
import com.peller.aizenberg.domain.repository.IMessageRepository
import com.peller.aizenberg.data.network.Api
import com.peller.aizenberg.domain.model.response.FCMResponse
import com.peller.aizenberg.domain.model.request.MessageBody
import com.peller.aizenberg.domain.model.request.MessageBodyInternal
import io.reactivex.Single

class MessageRepository(private val api: Api) : IMessageRepository {

    companion object {
        private const val API_KEY =
            "AAAAW4vE7rM:APA91bFnDIxwvIHChxzKHMwry3eVyW0gjlupnfSQMC7u1kbOrgB7z54r7DKiiELQru7SAophE_Rrzpded3EaLdTW1-nfUBxQFp1D9geKzD8_OOD4cikn8eWMVAMJKs14utrf5jYvgCiR"
        private const val API_KEY_FULL_HEADER = "key=$API_KEY"
        private const val APP_JSON_HEADER = "application/json"
        private const val API_TOPIC_NAME = "/topics/${Configuration.TOPIC_NAME}"
    }

    override fun send(body: String, from: String): Single<FCMResponse> {
        return api.sendMessage(
            MessageBody(
                MessageBodyInternal(
                    body,
                    from
                ),
                API_TOPIC_NAME
            ), APP_JSON_HEADER, API_KEY_FULL_HEADER
        )
    }
}