package com.peller.aizenberg.data.bot

import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.service.ServiceBridge

object BotManager : ServiceBridge.IBridgeListener {

    const val MAX_BOT_COUNT = 3
    private val connectedBots = HashSet<Bot>()

    fun botsCount() = connectedBots.count()

    fun canAdd() = connectedBots.count() < MAX_BOT_COUNT

    init {
        ServiceBridge.subscribe(this)
    }

    fun createBot(): Bot? {
        if (!canAdd()) return null
        val botDisplayName = BotNamesManager.randomName(connectedBots.map { it.botDisplayName })
        return Bot(botDisplayName, "Bot$botDisplayName").also { bot ->
            connectedBots.add(bot)
        }
    }

    fun destroyBot() : Bot? {
        if (connectedBots.isEmpty()) return null
        val bot = connectedBots.first()
        bot.dispose()
        connectedBots.remove(bot)
        return bot
    }

    //Deliver message to random bot
    override fun onMessageReceived(response: ServiceMessageResponse) {
        if (connectedBots.isNotEmpty()) {
            connectedBots.random().processIncomingMessage(response)
        }
    }

    override fun dispose() {

    }

}