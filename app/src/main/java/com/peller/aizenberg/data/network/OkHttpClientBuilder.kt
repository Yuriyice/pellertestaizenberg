package com.peller.aizenberg.data.network

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by Yuriy Aizenberg
 */
class OkHttpClientBuilder(private val httpLoggingInterceptor: Interceptor) {


    fun build(): OkHttpClient = buildClient()


    private fun buildClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
            .build()
    }


}