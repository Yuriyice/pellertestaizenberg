package com.peller.aizenberg.data.bot

import com.peller.aizenberg.di.KoinInjector
import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import com.peller.aizenberg.service.ServiceBridge
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class Bot(val botDisplayName: String, val botServiceName: String) {

    private val interactor = KoinInjector.instance.mainInteractor
    private val compositeDisposable = CompositeDisposable()

    fun processIncomingMessage(response: ServiceMessageResponse) {
        //Process only users message
        if (response.isMessageFromUser()) {
            sendDelayed(BotPhraseGenerator.randomTwoWords())
        }
    }

    private fun sendDelayed(botPhrase: String) {
        compositeDisposable.add(interactor.send(botPhrase, botDisplayName).delay(
            Random.nextInt(1, 5).toLong(), TimeUnit.SECONDS
        ).subscribe({
            Timber.d("Bot ($botDisplayName) success sent $botPhrase")
        }, {
            Timber.e(it)
        }))

    }

    fun dispose() {
        compositeDisposable.clear()
    }

}