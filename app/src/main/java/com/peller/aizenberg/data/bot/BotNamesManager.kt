package com.peller.aizenberg.data.bot

object BotNamesManager {

    private val names = arrayListOf("Bob", "Alice", "John")


    fun randomName(excluded: List<String>) : String {
        val namesCopy = ArrayList(names)
        excluded.forEach {
            namesCopy.remove(it)
        }
        return namesCopy.random()
    }

}