package com.peller.aizenberg.data.support

import com.google.gson.Gson
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Yuriy Aizenberg
 */
class GsonFactoryBuilder(private val gson: Gson) {

    fun build(): Converter.Factory {
        return GsonConverterFactory
                .create(gson)
    }

}