package com.peller.aizenberg.data.network

import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

/**
 * Created by Yuriy Aizenberg
 */
class RetrofitBuilder(
        private val gsonConverterFactory: Converter.Factory,
        private val client: OkHttpClient) {


    fun build(): Retrofit {
        return Retrofit.Builder()
                .validateEagerly(true)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl("https://fcm.googleapis.com")
                .client(client)
                .build()
    }


}