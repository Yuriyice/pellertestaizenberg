package com.peller.aizenberg.data.network

import com.peller.aizenberg.domain.model.response.FCMResponse
import com.peller.aizenberg.domain.model.request.MessageBody
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface Api {



    @POST("/fcm/send")
    fun sendMessage(@Body messageBody: MessageBody, @Header("content-type") contentType: String, @Header("authorization") authKey: String) : Single<FCMResponse>

}