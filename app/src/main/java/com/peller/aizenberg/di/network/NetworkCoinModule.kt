package com.peller.aizenberg.di.network

import com.peller.aizenberg.data.network.OkHttpClientBuilder
import com.peller.aizenberg.data.network.RetrofitBuilder
import org.koin.dsl.module
import retrofit2.Retrofit

object NetworkCoinModule {

    fun getModule() = module {
        single { OkHttpClientBuilder(get()).build() }
        single<Retrofit> { RetrofitBuilder(get(), get()).build() }
    }
}