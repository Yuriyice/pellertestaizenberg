package com.peller.aizenberg.di.interactor

import com.peller.aizenberg.domain.interactor.MainInteractor
import org.koin.dsl.module

object InteractorKoinModule {

    fun getModule() = module {
        single {MainInteractor(get())}
    }

}