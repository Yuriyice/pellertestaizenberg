package com.peller.aizenberg.di.network

import com.peller.aizenberg.data.network.Api
import org.koin.dsl.module
import retrofit2.Retrofit

object ApiKoinModule {

    fun getModule() = module {
        single<Api> { get<Retrofit>().create(Api::class.java) }
    }

}