package com.peller.aizenberg.di.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.peller.aizenberg.data.support.GsonFactoryBuilder
import eu.proxity.and.data.network.core.logging.HttpLoggingBuilder
import okhttp3.Interceptor
import org.koin.dsl.module
import retrofit2.Converter

object CommonNetworkKoinModule {

    fun getModule() = module {

        single<Interceptor> { HttpLoggingBuilder().build() }
        single<Gson> { GsonBuilder().create() }
        single<Converter.Factory> { GsonFactoryBuilder(
            get()
        ).build() }


    }

}