package com.peller.aizenberg.di.repository

import com.peller.aizenberg.data.repository.MessageRepository
import com.peller.aizenberg.domain.repository.IMessageRepository
import org.koin.dsl.module

object RepositoryKoinModule {

    fun getModule() = module {
        single<IMessageRepository> { MessageRepository(get()) }
    }

}