package com.peller.aizenberg.di

import com.peller.aizenberg.domain.interactor.MainInteractor
import org.koin.core.KoinComponent
import org.koin.core.inject

class KoinInjector private constructor() : KoinComponent {

    val mainInteractor : MainInteractor by inject()

    companion object {
        val instance = KoinInjector()
    }

}