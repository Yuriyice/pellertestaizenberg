package com.peller.aizenberg.service

import com.peller.aizenberg.domain.model.response.ServiceMessageResponse
import java.util.concurrent.CopyOnWriteArraySet

object ServiceBridge {

    private val listeners = CopyOnWriteArraySet<IBridgeListener>()

    fun subscribe(listener: IBridgeListener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener)
        }
    }

    fun unsubscibe(listener: IBridgeListener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener)
        }
    }


    fun dispose() {
        listeners.forEach {
            it.dispose()
        }
        listeners.clear()
    }

    fun notifyListeners(serviceMessageResponse: ServiceMessageResponse) {
        listeners.forEach {
            it.onMessageReceived(serviceMessageResponse)
        }
    }

    interface IBridgeListener {

        fun onMessageReceived(response: ServiceMessageResponse)

        fun dispose()
    }
}