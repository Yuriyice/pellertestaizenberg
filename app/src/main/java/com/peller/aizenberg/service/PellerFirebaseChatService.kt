package com.peller.aizenberg.service

import android.annotation.SuppressLint
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.peller.aizenberg.domain.model.response.ServiceMessageResponse

//We can ignore new token
@SuppressLint("MissingFirebaseInstanceTokenRefresh")
class PellerFirebaseChatService : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        ServiceMessageResponse.createFromRemoteMessage(p0)?.let {
            ServiceBridge.notifyListeners(it)
        }
    }


}